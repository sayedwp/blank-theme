Blank Theme
===

Blank Theme is a custom blank theme created with underscore theme, which includes foundation basic grid system and some basic customizer settings which are required in almost all projects.

Getting Started
---------------

1. Search for `'blank-theme'` (inside single quotations) to capture the text domain.
2. Search for `blank_theme_` to capture all the function names.
3. Search for `Text Domain: blank-theme` in style.css.
4. Search for <code>&nbsp;blank-theme</code> (with a space before it) to capture DocBlocks.
5. Search for `blank-theme-` to capture prefixed handles.
6. Search for `blanktheme` to to change the @package tag
7. Search for `blank theme`

Then, update the stylesheet header in `style.css` and the links in `footer.php` with your own information. Next, update or delete this readme.

Now you're ready to go! The next step is easy to say, but harder to do: make an awesome WordPress theme. :)

Good luck!